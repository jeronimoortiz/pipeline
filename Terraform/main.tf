# main.tf

# DynamoDB table for Terraform state locking
resource "aws_dynamodb_table" "terraform_locks" {
  name           = "terraform_locks"
  billing_mode   = "placeholder"
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

# S3 bucket for storing Terraform state files
resource "aws_s3_bucket" "terraform_state_bucket" {
  bucket = "placeholder"
  acl    = "private"

# Versioning in case You need to rollback the state file
  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

# Configure Terraform backend to store state in the created S3 bucket
terraform {
  backend "s3" {
    bucket         = aws_s3_bucket.terraform_state_bucket.bucket
    key            = "placeholder"
    region         = var.region
    dynamodb_table = aws_dynamodb_table.terraform_locks.name
    encrypt        = true
  }
}
