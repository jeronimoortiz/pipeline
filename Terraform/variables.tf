#
variable "aws_region" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}

# EC2 
variable "kms_key_arn_ebs" {}
variable "subnet_ec2" {}
variable "sg_ec2" {}
